var path = require("path");

var webpack = require("webpack");

module.exports = {
    entry : path.resolve(__dirname, 'js/index.js'),
    output : {
        path : path.resolve(__dirname, 'build/'),
        filename : 'index.js'
    },
    devtool : 'inline-source-map',
    module : {
        rules : [
            {
                test : /\.js$/,
                use : "babel-loader",
                exclude : path.resolve()
            },
            {
                test : /\.scss$/,
                use : [
                    {
                        loader : "style-loader"
                    },
                    {
                        loader : "css-loader"
                    }
                ]
            }
        ]
    }
};