# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

*A very basic dev environment for any sort of NPM-based web application.

### How do I get set up? ###

* CSS folder contains your CSS or SCSS code
* JS folder contains your JS code. Export other modules/files into index.js since Webpack bundles your code into that file.
* You can eventually put in an HTML folder if you want to have multiple pages. This setup assumes you're building an single page application (SPA).
* You can opt to include tests as you see fit.
* This comes with a headless browser called HTTP-server.
* After cloning this repo locally, install the node modules with npm init and then simply type npm run dev to build, watch for file changes and start the simple server.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact